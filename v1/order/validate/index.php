<?php

	require_once '../../../include/DBHandler.php';


	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}

	$inputJSON = file_get_contents('php://input');
	$arrayRequest = json_decode($inputJSON, TRUE); //convert JSON into array

	$response = array();
	$db = new DbHandler();

	$validated_products = array();

	$total = 0.0;

	foreach ($arrayRequest["products"] as $product){
		$quantity = $product["quantity"];
		$productInfo = $db->getProductById($product["id"]);
		if(!is_null($productInfo)){
			$productInfo["quantity"] = $quantity;
			$total_detail = $quantity * $productInfo["price"];
			$productInfo["total_detail"] = $total_detail;
			$total += $total_detail;

			$response["products"][] = $productInfo;
		}
	}

	$response["total"] = $total;
	utf8_encode_deep($response);


	header('Content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
	
	$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
	echo  $dataJSON;

	//var_dump($input);

?>