<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 */
class DbHandler {

    private $conn;

    function __construct() {

        /*define('DB_USERNAME', 'cherr927_giftsho');
        define('DB_PASSWORD', 'SxTXH3{MLAK1');
        define('DB_NAME', 'cherr927_giftshop');
        define('DB_HOST', 'localhost');

        $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        // Check for database connection error
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }*/


        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

 
    /**
     * Login
     * 
     * */
    public function login($email, $password) {
        $stmt = $this->conn->prepare("SELECT * FROM users where email = ? and password = ?");
		$stmt->bind_param("ss", $email, $password);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function emailExist($email){
        $stmt = $this->conn->prepare("SELECT * FROM users where email = ?");
        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            if($user){
                return true;
            }
        } else {
            $stmt->close();
        }

        return false;
    }


    /**
     * New user
     */
    public function registerUser($name, $email, $password, $phone) {

        $stmt = $this->conn->prepare("INSERT INTO users(name, photo, email, password, phone, is_admin) VALUES(?,?,?,?,?,?)") or die(mysql_error());
        $is_admin = 0;
        $photo = 'temp.jpg';
        $stmt->bind_param("sssssi", $name, $photo, $email ,$password ,$phone, $is_admin);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $id = $this->conn->insert_id;
            return array('id' => $id, 'name' => $name, 'photo' => $photo, 'email' => $email, 'password' => $password, 'phone' => $phone, 'is_admin' => $is_admin);
        } else {
            // task failed to create
            return NULL;
        }
    }


    /**
     * Listing categories
     * 
     * */
    public function getAllCategories() {
        $stmt = $this->conn->prepare("SELECT * FROM categories");
        $stmt->execute();
        $categories = array();
        $categories[] = array('id' => 0, 'description' => 'Todos');
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc())
        {
            $categories[] = $row;
        }
        $stmt->close();
        return $categories;
    }


    /**
     * Listing categories
     * 
     * */
    public function getAllProducts() {
        require_once dirname(__FILE__) . '/Config.php';

        $stmt = $this->conn->prepare("SELECT PRO.*, CAT.description as 'category_name' FROM products as PRO JOIN categories AS CAT on PRO.category = CAT.id WHERE PRO.active = 1");
        $stmt->execute();
        $products = array();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc())
        {
            //$row['url_image'] = URL_IMAGES_PRODUCTS . $row['image'];
            //$row['url_image'] = $row['image'];
            $products[] = $row;
        }
        $stmt->close();
        return $products;
    }


    /**
     * New user
     */
    public function newProduct($name, $price, $description, $sku, $category, $image) {

        $stmt = $this->conn->prepare("INSERT INTO products(name, price, description, image, sku, category, active) VALUES(?,?,?,?,?,?,?)") or die(mysql_error());
        $active = 1;
        $stmt->bind_param("sdsssii", $name, $price, $description ,$image ,$sku, $category, $active);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $id = $this->conn->insert_id;
            return array('id' => $id);
        } else {
            // task failed to create
            return NULL;
        }
    }

    public function updateProduct($name, $price, $description, $sku, $category, $image, $id) {

        $stmt = $this->conn->prepare("UPDATE products SET name = ?, price = ?, description = ?, image = ?, sku = ?, category = ? WHERE id = ?") or die(mysql_error());
        $stmt->bind_param("sdsssii", $name, $price, $description ,$image ,$sku, $category, $id);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            //$id = $this->conn->insert_id;
            return array('id' => $id);
        } else {
            // task failed to create
            return NULL;
        }
    }

    public function getProductById($id) {
        require_once dirname(__FILE__) . '/Config.php';

        $stmt = $this->conn->prepare("SELECT PRO.*, CAT.description as 'category_name' FROM products as PRO JOIN categories AS CAT on PRO.category = CAT.id WHERE PRO.id = ? AND PRO.active = 1");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $product = array();
        $result = $stmt->get_result();

        if($result){
            $product = $result->fetch_assoc();
        }

        $stmt->close();
        return $product;
    }

    public function saveOrder($id_user, $total, $code, $cardname_holder, $card_number, $card_type, $expiration_date, $ccv)
    {
        $stmt = $this->conn->prepare("INSERT INTO orders(id_user, total, code, cardname_holder, card_number, card_type, expiration_date, ccv) VALUES(?,?,?,?,?,?,?,?)") or die(mysql_error());
        $stmt->bind_param("idssssss", $id_user, $total, $code, $cardname_holder, $card_number, $card_type, $expiration_date, $ccv);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $id = $this->conn->insert_id;
            return $id;
        } else {
            // task failed to create
            return NULL;
        }
    }

    public function saveOrderDetail($id_order, $id_product, $price, $quantity)
    {
        $stmt = $this->conn->prepare("INSERT INTO order_details(id_order, id_product, price, quantity) VALUES(?,?,?,?)") or die(mysql_error());
        $stmt->bind_param("iidi", $id_order, $id_product, $price, $quantity);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $id = $this->conn->insert_id;
            return $id;
        } else {
            // task failed to create
            return NULL;
        }
    }

    public function deleteProduct($id_product)
    {
        $stmt = $this->conn->prepare("UPDATE products SET active = 0 WHERE id = ?") or die(mysql_error());
        $stmt->bind_param("i", $id_product);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            return true;
        } else {
            // task failed to create
            return false;
        }

    }

    public function getProductByFilter($query){
        require_once dirname(__FILE__) . '/Config.php';

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $products = array();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc())
        {
            //$row['url_image'] = URL_IMAGES_PRODUCTS . $row['image'];
            //$row['url_image'] = $row['image'];
            $products[] = $row;
        }
        $stmt->close();
        return $products;
    }

    public function countOrderFilter($query){
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $products = array();
        $result = $stmt->get_result();

        if($row = $result->fetch_assoc()){
            return $row['total'];
        }else{
            return 0;
        }
    }


}

?>