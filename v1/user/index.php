<?php 
	
	require_once '../../include/DBHandler.php';

	date_default_timezone_set('America/Hermosillo');

	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{

		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$phone = $_POST['phone'];

		$response = array();
        $db = new DbHandler();

        $email_exist = $db->emailExist($email);

        if(!$email_exist)
        {

	        $result = $db->registerUser($name, $email, $password, $phone);

	        if(!is_null($result)){

	        	header("HTTP/1.1 201 Created");
	        	$response = $result;

	        }else{

	        	header("HTTP/1.1 400 - Bad Request");
				$response['code'] = 400;
	        	$response['error'] = true;
				$response['message'] = 'Ocurrió un error al realizar el registro, inténtelo más tarde';

	        }
        }
        else
        {

        	header("HTTP/1.1 400 - Bad Request");
			$response['code'] = 400;
        	$response['error'] = true;
			$response['message'] = 'El correo ya está registrado';

        }

	}
	else
	{
		header("HTTP/1.1 500 Server Error");
		$response = array();
		$response['code'] = 500;
		$response['error'] = true;
		$response['message'] = 'Solicitud incorrecta';
	}

	header('Content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");


	
	$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
	echo  $dataJSON;

?>