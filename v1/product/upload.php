<?php

	date_default_timezone_set('America/Hermosillo');


	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
		
		ini_set('upload_max_filesize', '10M');
		ini_set('post_max_size', '10M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);

		include '../../libs/Intervention/Image/Image.php';

		//$ruta_fotos = "../../images/products/";
		//$ruta_fotos_temp = "../../images/products/temp/";

		$ruta_fotos = "images/";
		$ruta_fotos_temp = "images/temp/";
		$valid_formats_for_images = array("jpg","jpeg","png");

		$image = '';

		
		if( isset($_FILES['image']) )
		{
			$name = $_FILES['image']['name'];
			list($txt, $ext) = explode(".", $name);	

			if(strlen($name)){
				$actual_image_name = time().rand().".".$ext; //name from the file
				$tmp = $_FILES['image']['tmp_name'];

				if(move_uploaded_file($tmp, $ruta_fotos_temp.$actual_image_name)){
					
					if( file_exists($ruta_fotos_temp.$actual_image_name) ){

						//generate the thumb
						$thumbWidth = 600; // Width of the thumbnails generated.
						$picture = $ruta_fotos_temp.$actual_image_name;
			  			$thumb = $ruta_fotos.$actual_image_name;
			  			$img = Intervention\Image\Image::make($picture);
			  			$img->resize($thumbWidth, null, function ($constraint) {
							$constraint->aspectRatio();
						});
						// finally we save the image as a new image
						$img->save($thumb);

						$image = $actual_image_name;

						unlink($ruta_fotos_temp.$actual_image_name);

					}

				}

			}

		}


		require_once '../../include/Config.php';

		//save the product database
		$response = array('image' => URL_IMAGES_PRODUCTS . $image);
        //$db = new DbHandler();

		//$response = $image;

        /*$result = $db->newProduct($name, $price, $description, $sku, $category, $image);

        if(!is_null($result)){

        	header("HTTP/1.1 201 Created");
        	$response = $result;

        }else{

        	header("HTTP/1.1 400 - Bad Request");
			$response['code'] = 400;
        	$response['error'] = true;
			$response['message'] = 'Ocurrió un error al agregar el producto, inténtelo más tarde';

        }
        */

        header('Content-type: application/json; charset=utf-8');
	    header("access-control-allow-origin: *");
		
		$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
		echo  $dataJSON;

	}

?>