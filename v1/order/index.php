<?php

	require_once '../../include/DBHandler.php';

	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}

	$inputJSON = file_get_contents('php://input');
	$arrayRequest = json_decode($inputJSON, TRUE); //convert JSON into array

	$response = array();
	$db = new DbHandler();

	$id_user = $arrayRequest["id_user"];
	$total = $arrayRequest["total"];
	$code = $arrayRequest["code"];
	$cardname_holder = $arrayRequest["cardname_holder"];
	$card_number = $arrayRequest["card_number"];
	$card_type = $arrayRequest["card_type"];
	$expiration_date = $arrayRequest["expiration_date"];
	$ccv = $arrayRequest["ccv"];
	$random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) );
	$code = $random_number;

	$id_order = $db->saveOrder($id_user, $total, $code, $cardname_holder, $card_number, $card_type, $expiration_date, $ccv);

	if(!is_null($id_order))
	{

		foreach ($arrayRequest["products"] as $product)
		{

			$id_product = $product["id"];
			$price = $product["price"];
			$quantity = $product["quantity"];
			$total_detail = $product["total_detail"];

			$id_detail = $db->saveOrderDetail($id_order, $id_product, $total_detail, $quantity);
			
		}

	}

	$arrayRequest["code"] = $code;

	$response  = $arrayRequest;
	utf8_encode_deep($response);

	header("HTTP/1.1 201 Created");

	header('Content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
	
	$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
	echo  $dataJSON;
?>