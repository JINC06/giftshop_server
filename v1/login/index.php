<?php

	require_once '../../include/DBHandler.php';

	date_default_timezone_set('America/Hermosillo');

	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}

	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{

		$email =  $_POST['email'];
		$password = $_POST['password'];

		$response = array();
        $db = new DbHandler();

        $result = $db->login($email, $password);

        if(!is_null($result)){

        	header("HTTP/1.1 200 OK");
        	$response = $result;
        	utf8_encode_deep($response);

        }else{

			header("HTTP/1.1 401 - Unauthorized");
			$response['code'] = 401;
        	$response['error'] = true;
			$response['message'] = 'Usuario y/o contraseña incorrectos';

        }

	}
	else
	{
		header("HTTP/1.1 500 Server Error");
		$response = array();
		$response['code'] = 500;
		$response['error'] = true;
		$response['message'] = 'Solicitud incorrecta';
	}

	header('Content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");


	
	$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
	echo  $dataJSON;
?>