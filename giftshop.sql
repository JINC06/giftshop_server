-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2017 at 04:09 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `giftshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `description`) VALUES
(1, 'Dulces'),
(2, 'Ropa'),
(3, 'Juguetes'),
(4, 'Postales'),
(5, 'Varios'),
(6, 'Libros');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `total` decimal(7,2) NOT NULL,
  `code` varchar(50) NOT NULL,
  `cardname_holder` varchar(100) NOT NULL,
  `card_number` varchar(20) NOT NULL,
  `card_type` varchar(20) NOT NULL,
  `expiration_date` varchar(10) NOT NULL,
  `ccv` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `id_user`, `total`, `code`, `cardname_holder`, `card_number`, `card_type`, `expiration_date`, `ccv`) VALUES
(5, 2, '2090.40', '99914', 'Jose Alvarez', '4111111111111111', 'VISA', '12/20', '123');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `id_order`, `id_product`, `price`, `quantity`) VALUES
(10, 5, 5, '40.00', '2.00'),
(11, 5, 8, '2000.00', '1.00'),
(12, 5, 6, '50.40', '1.00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `sku` text NOT NULL,
  `category` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `image`, `sku`, `category`, `created_at`, `active`) VALUES
(5, 'Jamoncillo', '20.00', 'Jamoncillo is a traditional Mexican candy, specifically from the states of Nuevo Leon, Coahuila, Durango, Chihuahua, Sinaloa, Sonora, Jalisco and the State of Mexico, made with milk and sugar. It is light brown in color and can be made mixed with chopped walnut.', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU!12331', 1, '2017-11-18 04:19:10', 1),
(6, 'Harry Potter (Literary Series)', '50.40', 'Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels chronicle the life of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU898989', 6, '2017-11-18 20:24:28', 1),
(7, 'Android telefono 2', '2000.00', 'New cellphone very good', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU10000', 5, '2017-11-24 04:29:17', 1),
(8, 'Android télefono', '2000.00', 'New cellphone very good', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9999', 5, '2017-11-24 04:32:32', 1),
(9, 'Android telefono', '2345.00', 'New cellphone very good', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-24 12:50:18', 0),
(10, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:12', 1),
(11, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:13', 1),
(12, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:13', 1),
(13, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:14', 1),
(14, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:14', 1),
(15, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:14', 1),
(16, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:14', 1),
(17, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:15', 1),
(18, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:15', 1),
(19, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:15', 1),
(20, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:15', 1),
(21, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:15', 1),
(22, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:16', 1),
(23, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:16', 1),
(24, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:16', 1),
(25, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:16', 1),
(26, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:16', 1),
(27, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(28, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(29, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(30, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(31, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(32, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:17', 1),
(33, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:18', 1),
(34, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:18', 1),
(35, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:18', 1),
(36, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:19', 1),
(37, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:19', 1),
(38, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:19', 1),
(39, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:19', 1),
(40, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:19', 1),
(41, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:20', 1),
(42, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:20', 1),
(43, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:20', 1),
(44, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:20', 1),
(45, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:20', 1),
(46, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:21', 1),
(47, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:21', 1),
(48, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:21', 1),
(49, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:21', 1),
(50, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:22', 1),
(51, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:22', 1),
(52, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:22', 1),
(53, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 02:45:22', 1),
(54, 'Android telefono 3', '2100.00', 'New cellphone', 'http://giftshop.pitalla.mx/v1/product/images/1511527766601740867.jpg', 'SKU9991', 5, '2017-11-25 13:33:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `password`, `phone`, `is_admin`) VALUES
(1, 'Julio Nava', 'julionava.jpg', 'julio.nava06@gmail.com', '123456', '6221002760', 1),
(2, 'Jose Alvarez', 'jose.jpg', 'josealvarez@gmail.com', '123456', '6221002761', 0),
(9, 'Julio Nava', 'temp.jpg', 'julio.nava08@gmail.com', '1234567890', '6221002761', 0),
(10, 'Julio Nava', 'temp.jpg', 'julio.nava09@gmail.com', '1234567890', '6221002761', 0),
(11, 'Julio Nava Registro App', 'temp.jpg', 'julio.nava19@gmail.com', '123456', '6221002759', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_products_categories` (`category`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_products_categories` FOREIGN KEY (`category`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
