<?php
	
	require_once '../../include/DBHandler.php';

	date_default_timezone_set('America/Hermosillo');


	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}


	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
		
		ini_set('upload_max_filesize', '10M');
		ini_set('post_max_size', '10M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);

		include '../../libs/Intervention/Image/Image.php';

		$ruta_fotos = "../../images/products";
		$ruta_fotos_temp = "../../images/products/temp";
		$valid_formats_for_images = array("jpg","jpeg","png");

		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$sku = $_POST['sku'];
		$category = $_POST['category'];
		$image = $_POST['image'];

		//save the product database
		$response = array();
        $db = new DbHandler();

        $result = $db->newProduct($name, $price, $description, $sku, $category, $image);

        if(!is_null($result)){

        	header("HTTP/1.1 201 Created");
        	$response = $result;

        }else{

        	header("HTTP/1.1 400 - Bad Request");
			$response['code'] = 400;
        	$response['error'] = true;
			$response['message'] = 'Ocurrió un error al agregar el producto, inténtelo más tarde';

        }

        header('Content-type: application/json; charset=utf-8');
	    header("access-control-allow-origin: *");
		
		$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
		echo  $dataJSON;

	}


	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "GET")
	{

		$response = array();
		$db = new DbHandler();

		$id_category = isset($_GET['id_category']) ? $_GET['id_category'] : 0;
		$search_query = isset($_GET['search_query']) ? $_GET['search_query'] : '';
		$search_query = strtolower($search_query);

		//cuantos hay por pagina
		$per_page = isset($_GET['per_page']) ? $_GET['per_page'] : 5;
		//en que pagina vamos
		$current_page = isset($_GET['current_page']) ? $_GET['current_page'] : 1;


		$where_data = "";
		if($id_category != 0)
		{
			$where_data .= "PRO.category = ".$id_category;
		}

		if(!empty($search_query))
		{
			if(!empty($where_data))
			{
				$where_data .= " AND LOWER(CONCAT(PRO.name, ' ', PRO.description)) like '%".$search_query."%'";
			}else{
				$where_data = "LOWER(CONCAT(PRO.name, ' ', PRO.description)) like '%".$search_query."%'";
			}
		}

		if(!empty($where_data))
		{
			$where_data .= " AND PRO.active = 1";
		}else{
			$where_data = "PRO.active = 1";
		}

		$query = "SELECT PRO.*, CAT.description as 'category_name' FROM products as PRO JOIN categories AS CAT on PRO.category = CAT.id WHERE {WHERE}";

		$query_count = "SELECT COUNT(*) as total FROM products as PRO JOIN categories AS CAT on PRO.category = CAT.id WHERE {WHERE}";

		$query = str_replace("{WHERE}", $where_data, $query);
		$query_count = str_replace("{WHERE}", $where_data, $query_count);

		$total_results = $db->countOrderFilter($query_count);

		$pages_temp = $total_results / $per_page;
		$whole = floor($pages_temp); 
		$fraction = $pages_temp - $whole;

		if($fraction > 0){
			$pages = $whole + 1;
		}else{
			$pages = $pages_temp;
		}

		$start = $current_page;
		if($start > 1){
			$start = (($start-1) * $per_page);
		}else{
			$start = 0;
		}

		$limit_data = "LIMIT ".$start.",".$per_page;
		$query_with_limit = str_replace("{LIMIT}", $limit_data, $query." {LIMIT}");

		$results = $db->getProductByFilter($query_with_limit);

		header("HTTP/1.1 200 OK");
        
        //$response = $db->getAllProducts();
        //utf8_encode_deep($response);
        $response = array(
        	//'query' => $query, 
        	//'query_count' => $query_count, 
        	'total_results' => $total_results, 
        	'pages' => $pages, 
        	//'query_with_limit' => $query_with_limit, 
        	'current_page' => $current_page, 
        	'per_page' => $per_page,
        	'results' => $results
        );
        utf8_encode_deep($response);

        header('Content-type: application/json; charset=utf-8');
	    header("access-control-allow-origin: *");
		
		$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
		echo  $dataJSON;

	}

	if(isset($_GET['id']) and $_SERVER['REQUEST_METHOD'] == "DELETE")
	{
		$response = array();
		$db = new DbHandler();

		$id_product = $_GET['id'];

		if($db->deleteProduct($id_product)){
			header("HTTP/1.1 200 OK");
			$response['code'] = 200;
        	$response['error'] = false;
			$response['message'] = 'Producto eliminado correctamente.';
		}else{
			header("HTTP/1.1 500 Server Error");
			$response['code'] = 500;
        	$response['error'] = true;
			$response['message'] = 'Ocurrió un error al eliminar el producto.';

		}

		utf8_encode_deep($response);

		header('Content-type: application/json; charset=utf-8');
	    header("access-control-allow-origin: *");
		
		$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
		echo  $dataJSON;
	}

?>