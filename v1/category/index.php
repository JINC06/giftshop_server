<?php
	
	require_once '../../include/DBHandler.php';

	date_default_timezone_set('America/Hermosillo');

	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}

	$response = array();

	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "GET")
	{
	
        $db = new DbHandler();

		header("HTTP/1.1 200 OK");
        $response = $db->getAllCategories();
        utf8_encode_deep($response);

	}else{

		header("HTTP/1.1 400 Not Found");

	}


	header('Content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
	
	$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
	echo  $dataJSON;

?>