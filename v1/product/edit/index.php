<?php
	
	require_once '../../../include/DBHandler.php';

	date_default_timezone_set('America/Hermosillo');


	function utf8_encode_deep(&$input) {
	    if (is_string($input)) {
	        $input = utf8_encode($input);
	    } else if (is_array($input)) {
	        foreach ($input as &$value) {
	            utf8_encode_deep($value);
	        }

	        unset($value);
	    } else if (is_object($input)) {
	        $vars = array_keys(get_object_vars($input));

	        foreach ($vars as $var) {
	            utf8_encode_deep($input->$var);
	        }
	    }
	}


	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
		
		ini_set('upload_max_filesize', '10M');
		ini_set('post_max_size', '10M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);

		include '../../libs/Intervention/Image/Image.php';

		$ruta_fotos = "../../images/products";
		$ruta_fotos_temp = "../../images/products/temp";
		$valid_formats_for_images = array("jpg","jpeg","png");

		$id = $_POST['id'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$sku = $_POST['sku'];
		$category = $_POST['category'];
		$image = $_POST['image'];

		//save the product database
		$response = array();
        $db = new DbHandler();

        $result = $db->updateProduct($name, $price, $description, $sku, $category, $image, $id);

        if(!is_null($result)){

        	header("HTTP/1.1 201 Created");
        	$response = $result;

        }else{

        	header("HTTP/1.1 400 - Bad Request");
			$response['code'] = 400;
        	$response['error'] = true;
			$response['message'] = 'Ocurrió un error al agregar el producto, inténtelo más tarde';

        }

        header('Content-type: application/json; charset=utf-8');
	    header("access-control-allow-origin: *");
		
		$dataJSON = json_encode($response,JSON_UNESCAPED_UNICODE);
		echo  $dataJSON;

	}
	

?>